
@include('front/inc/header')
<!-- BREADCRUMB -->
<div id="breadcrumb">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">{{$content->title}}</li>
    </ul>
  </div>
</div>
<!-- /BREADCRUMB -->

<!-- section -->
<div class="section">
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="listing-title text-center">
      <h4>{{isset($content->title) ? $content->title : ''}}</h4>
    </div>

    <div class="col-sm-10 offset-sm-1" style="padding: 2%;border: 1px solid #ddd">
      {!!isset($content->description) ? $content->description : ''!!}


    </div>
  </div>
  <!-- /container -->
</div>
<!-- /section -->

@include('front/inc/footer')