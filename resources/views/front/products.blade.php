
@include('front/inc/header')

	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="#">Home</a></li>
				<li class="">Products</li>
                @if(isset($keyword))
                    <li class="active"><a href="">{{$keyword}}</a></li>
                @endif
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- ASIDE -->
				<div id="aside" class="col-md-3">

					<!-- aside widget -->
					<div class="aside">
						<h3 class="aside-title">Filter by Price</h3>
						<div id="price-slider"></div>
					</div>

					<!-- aside widget -->
					<div class="aside">
						<h3 class="aside-title">Releted Search</h3>
						<ul class="list-links">
							@foreach($related_keywords as $related_keyword)
								@if($related_keyword=='lorem')
									@continue
								@endif
								<li><a href="{{url('search/'.str_replace(' ','-',$related_keyword))}}">{{$related_keyword}}</a></li>
							@endforeach
						</ul>
					</div>
					<!-- /aside widget -->


				</div>
				<!-- /ASIDE -->

				<!-- MAIN -->
				<div id="main" class="col-md-9">
                    @if(sizeof($products) < 1)
                        <div class=" col-sm-12 text-center text-danger" style="padding:80px;border: 1px solid #ddd;">
                            <h4><i class="fa fa-exclamation-triangle"></i> <strong>Sorry</strong>! No Matchs Found </h4>

                        </div>
                    @else
					<!-- store top filter -->
					<div class="store-filter clearfix">
						<div class="pull-left">
							<div class="row-filter">
								<a href="#"><i class="fa fa-th-large"></i></a>
								<a href="#" class="active"><i class="fa fa-bars"></i></a>
							</div>
							<div class="sort-filter">
								<span class="text-uppercase">Sort By:</span>
								<select class="input">
									<option value="{{url('product/products').'?product_sort=AZ'}}">Name (A - Z)</option>
									<option value="{{url('product/products').'?product_sort=ZA'}}">Name (Z - A)</option>
									<option value="{{url('product/products').'?product_sort=LH'}}">Price (Low &gt; High)</option>
									<option value="{{url('product/products').'?product_sort=HL'}}">Price (High &gt; Low)</option>
								</select>
							</div>

                            <div class="page-filter" style="margin-left: 4em;">
                                <span class="text-uppercase">Show:</span>
                                <select class="input">
                                    <option value="0">18</option>
                                    <option value="1">30</option>
                                    <option value="2">40</option>
                                </select>
                            </div>
						</div>
						<div class="pull-right">

                            <div class="text-left">
                                {!! $products->render() !!}
                            </div>

						</div>
					</div>
					<!-- /store top filter -->

					<!-- STORE -->
					<div id="store">
						<!-- row -->
						<div class="row">
                            @foreach($products as $key=>$item)
                            <!-- Product Single -->
                            <div class="col-md-4 col-sm-6 col-xs-6" style="height: 450px;">
                                <div class="product product-single">
                                    <div class="product-thumb text-center">
                                        <button class="main-btn quick-view"  data-toggle="modal" data-target="#productModal{{$item->id}}"><i class="fa fa-search-plus"></i> Quick view</button>
                                        <img class="img-responsive center-block" src="{{$item->image ? $item->image : asset('img/default-image.jpg')}}" alt="" style="height: 200px;width: auto; display: block;margin:0 auto;">
                                    </div>
                                    <div class="product-body">
										<p class="product-name"><a href="#" data-toggle="modal" data-target="#productModal{{$item->id}}">{{$item->title}}</a></p>
                                        <div class="col-md-6  col-sm-6">
											<h3 class="product-price">
												@if($item->discount)
													$ {{$item->price- $item->discount}}
													<del class="product-old-price"> $  {{$item->price}}</del>
												@else
													$ {{$item->price}}
												@endif
											</h3>
										</div>
										<div class="col-md-6  col-sm-6" style="width: 50%;">
											<img src="{{asset($item->merchant->logo)}}" alt="" class="main-btn add-to-cart" style="width: 100%;">
										</div>
										<div class="clearfix"></div>
                                        <div class="product-btns">
                                            <button class="main-btn add-to-cart"><i class="fa fa-heart"> Wishlist</i></button>
                                            <!--<a class="primary-btn add-to-cart" target="_blank" href="{{$item->url}}"><i class="fa fa-shopping-cart"></i> Buy Now</a>-->
                                            <a class="primary-btn add-to-cart" onclick="ProductRedirect({{$item->id}})"><i class="fa fa-shopping-cart"></i> Buy Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Product Single -->

							<!--Modal -->
							<div class="modal fade" id="productModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header text-success">
											<h5 class="modal-title" id="exampleModalLabel">{{$item->title}}</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">

											<div class="product product-details clearfix">
												<div class="col-md-6">
													<div id="product-main-view">
														<img src="{{$item->image ? $item->image : asset('img/default-image.jpg')}}" alt="" style="max-width: 90%;display: block;margin:0 auto;">
													</div>
												</div>

												<div class="col-md-6">
													<div class="product-body">
														<h4 class="product-name">{{$item->title}}</h4>
														<h3 class="product-price">
															@if($item->discount)
																$ {{$item->price- $item->discount}}
																<del class="product-old-price"> $  {{$item->price}}</del>
															@else
																$ {{$item->price}}
															@endif
														</h3>
														<p><strong>Shop :</strong> {{$item->merchant->name}}</p>
														<img src="{{asset($item->merchant->logo)}}" alt="{{$item->merchant->name}}" title="{{$item->merchant->name}}" style="width: 120px;">
														<p>{{$item->description}}</p>

														<br>

														<div class="product-btns">
															<button class="main-btn add-to-cart"><i class="fa fa-heart"> Wishlist</i></button>
															<!--<a class="primary-btn add-to-cart" target="_blank" href="{{$item->url}}"><i class="fa fa-shopping-cart"></i> Buy Now</a>-->
															<a class="primary-btn add-to-cart" onclick="ProductRedirect({{$item->id}})"><i class="fa fa-shopping-cart"></i> Buy Now</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
								<!--End Modal -->

                            @endforeach
						</div>
						<!-- /row -->
					</div>
					<!-- /STORE -->

					<!-- store bottom filter -->
					<div class="store-filter clearfix">
                        <div class="pull-left">
                            <div class="row-filter">
                                <a href="#"><i class="fa fa-th-large"></i></a>
                                <a href="#" class="active"><i class="fa fa-bars"></i></a>
                            </div>
                            <div class="sort-filter">
                                <span class="text-uppercase">Sort By:</span>
                                <select class="input">
                                    <option value="{{url('product/products').'?product_sort=AZ'}}">Name (A - Z)</option>
                                    <option value="{{url('product/products').'?product_sort=ZA'}}">Name (Z - A)</option>
                                    <option value="{{url('product/products').'?product_sort=LH'}}">Price (Low &gt; High)</option>
                                    <option value="{{url('product/products').'?product_sort=HL'}}">Price (High &gt; Low)</option>
                                </select>
                            </div>

                            <div class="page-filter" style="margin-left: 4em;">
                                <span class="text-uppercase">Show:</span>
                                <select class="input">
                                    <option value="0">18</option>
                                    <option value="1">30</option>
                                    <option value="2">40</option>
                                </select>
                            </div>
                        </div>
                        <div class="pull-right">

                            <div class="text-left">
                                {!! $products->render() !!}
                            </div>

                        </div>
					</div>

                    @endif
					<!-- /store bottom filter -->
				</div>
				<!-- /MAIN -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

<style>
    .pagination{
        margin: 0px;
    }
</style>
@include('front/inc/footer')