
@include('front/inc/header')
<!-- BREADCRUMB -->
<div id="breadcrumb">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li class="active">{{$content->title}}</li>
    </ul>
  </div>
</div>
<!-- /BREADCRUMB -->

<!-- section -->
<div class="section">
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="col-md-6 offset-md-3">

      @if(session()->has('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
      @endif

      @if($errors->any())
        <div class="alert alert-danger">
          {{ $errors->first() }}
        </div>
      @endif
      <form action="{{url('inquery/submit')}}" method="POST">
        {{csrf_field()}}
          <h4 class="text-uppercase">Write Your Review</h4>
          <p>Your email address will not be published.</p>
            <div class="form-group">
              <input class="input" name="name" type="text" placeholder="Your Name">
            </div>
            <div class="form-group">
              <input class="input" type="email" name="email" placeholder="Email Address">
            </div>
            <div class="form-group">
              <textarea name="message" class="input" style="height: 100px;" placeholder="Your Message"></textarea>
            </div>
            <button class="primary-btn">Submit</button>
      </form>
    </div>
  </div>
  <!-- /container -->
</div>
<!-- /section -->

@include('front/inc/footer')