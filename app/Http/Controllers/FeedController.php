<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;

class FeedController extends Controller
{
    //

    public function index()
    {
        $merchants = User::where('type','merchant')->where('status','active')->get();
        return view('admin.feed.create',compact('merchants'));
    }

    public function insert()
    {
        $merchants = User::where('type','merchant')->where('status','active')->get();
        return view('admin.feed.create',compact('merchants'));
    }


    public function Submit(Request $request){

        $this->validate($request, [
            'merchant_id' => 'required',
            'mode' => 'required',
        ]);

        $merchant_id = $request->merchant_id;
        $upload_mode = $request->mode;

        if ($upload_mode=='replace')
        {
            Product::where('merchant_id',$merchant_id)->delete();
        }

        //clean
        Product::where('merchant_id',0)->delete();

        if ($request->hasFile('feed'))
        {
            $xmlfile = $request->file('feed');;$data[]=array();
            $tagname ='item';

            $dom = new \DOMDocument();
            $dom->load($xmlfile);
            $rows = $dom->getElementsByTagName($tagname);
            $first_row = true;
            $count=0;
            foreach ($rows as $row)
            {
                $product = new Product();
                $product->ref = uniqid();
                $product->merchant_id = $merchant_id;
                $product->status = 'active';


                $cells_title = $row->getElementsByTagName( 'title' );
                $cells_url = $row->getElementsByTagName( 'url' );
                $cells_desc = $row->getElementsByTagName( 'description' );
                $cells_image = $row->getElementsByTagName( 'image' );
                $cells_price = $row->getElementsByTagName( 'price' );


                if (isset($cells_title[0]->nodeValue))
                {
                    $product->title = $cells_title[0]->nodeValue;
                }
                if (isset($cells_url[0]->nodeValue))
                {
                    $product->url = $cells_url[0]->nodeValue;
                }
                if (isset($cells_desc[0]->nodeValue))
                {
                    $product->description = $cells_desc[0]->nodeValue;
                }
                if (isset($cells_image[0]->nodeValue))
                {
                    $product->image = $cells_image[0]->nodeValue;
                }
                if (isset($cells_price[0]->nodeValue))
                {
                    $product->price = $cells_price[0]->nodeValue;
                }

                $product->save();

                $count++;


            }


        }
        return back()->withSuccess('Feed Successfully Updated! Total '.$count.' Product Updated!');

    }
}
